'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl',
    function AnswerController($scope, $http){

        $scope.response={};
        $scope.save = function (answer, answerForm){
            if(answerForm.$valid){

                $http.post("user.json", answer).success(function (answ, status) {
                    $scope.response=answ;
                    console.log(answer);
                    $scope.status = status;
                    console.log(status);
                });
            }
        };
    }
);