'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl',
    function AnswerController($scope, $http){

        $scope.response={};
        $scope.save = function (answer, answerForm){
            if(answerForm.$valid){

                $http.post("user.json", answer).success(function (answ, status) {
                    $scope.response=answ;
                    console.log(answer);
                    $scope.status = status;
                    console.log(status);
                });
            }
        };
    }
);